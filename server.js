// require('dotenv').config();
// const pgp = require('pg-promise')({});

// const log4js = require('log4js');

// log4js.configure({
//     appenders: { DB: { type: 'file', filename: 'log.log' } },
//     categories: { default: { appenders: ['DB'], level: 'trace' } },
// });

// const logger = log4js.getLogger('DB');

// const end_point_cn = {
//     host: process.env.DB_HOST,
//     port: process.env.DB_PORT,
//     database: process.env.DB_END_POINT_DB_NAME,
//     user: process.env.DB_USER,
//     password: process.env.DB_PASS,
// };

// const work_cn = {
//     host: process.env.DB_HOST,
//     port: process.env.DB_PORT,
//     database: process.env.DB_WORK_DB_NAME,
//     user: process.env.DB_USER,
//     password: process.env.DB_PASS,
// };

// const players_cn = {
//     host: process.env.DB_HOST,
//     port: process.env.DB_PORT,
//     database: process.env.DB_PLAYERS_DB_NAME,
//     user: process.env.DB_USER,
//     password: process.env.DB_PASS,
// };

// const end_point_db = pgp(end_point_cn);
// const work_db = pgp(work_cn);
// const players_db = pgp(players_cn);


// function insertNewPlayersToGlobalPlayersDB(player) {
//     return end_point_db.any(`
//         insert into players (playername, pokersite_id)
//         select $1, $2
//         where not exists (select playername, pokersite_id from players where playername = $1 and pokersite_id = $2)
//     `, [player.playername, player.pokersite_id]);
// }

// function insertOneCompiledPlayerResult(result) {
//     return end_point_db.any(`
//     INSERT INTO ${process.env.COMPILED_RESULTS_TABLE_NAME}
//         (player_id, tablesize, bigblindincents, playedyearandmonth, currencytype_id, pokergametype_id,
//             totalhands,
//             totalamountwonincents,
//             totalrakeincents,
//             totalbbswon,
//             vpiphands,
//             pfrhands,
//             couldcoldcall,
//             didcoldcall,
//             couldthreebet,
//             didthreebet,
//             couldsqueeze,
//             didsqueeze,
//             facingtwopreflopraisers,
//             calledtwopreflopraisers,
//             raisedtwopreflopraisers,
//             smallblindstealattempted,
//             smallblindstealdefended,
//             smallblindstealreraised,
//             bigblindstealattempted,
//             bigblindstealdefended,
//             bigblindstealreraised,
//             sawnonsmallshowdown,
//             wonnonsmallshowdown,
//             sawlargeshowdown,
//             wonlargeshowdown,
//             sawnonsmallshowdownlimpedflop,
//             wonnonsmallshowdownlimpedflop,
//             sawlargeshowdownlimpedflop,
//             wonlargeshowdownlimpedflop,
//             wonhand,
//             wonhandwhensawflop,
//             wonhandwhensawturn,
//             wonhandwhensawriver,
//             facedthreebetpreflop,
//             foldedtothreebetpreflop,
//             calledthreebetpreflop,
//             raisedthreebetpreflop,
//             facedfourbetpreflop,
//             foldedtofourbetpreflop,
//             calledfourbetpreflop,
//             raisedfourbetpreflop,
//             turnfoldippassonflopcb,
//             turncallippassonflopcb,
//             turnraiseippassonflopcb,
//             riverfoldippassonturncb,
//             rivercallippassonturncb,
//             riverraiseippassonturncb,
//             sawflop,
//             sawshowdown,
//             wonshowdown,
//             totalbets,
//             totalcalls,
//             flopcontinuationbetpossible,
//             flopcontinuationbetmade,
//             turncontinuationbetpossible,
//             turncontinuationbetmade,
//             rivercontinuationbetpossible,
//             rivercontinuationbetmade,
//             facingflopcontinuationbet,
//             foldedtoflopcontinuationbet,
//             calledflopcontinuationbet,
//             raisedflopcontinuationbet,
//             facingturncontinuationbet,
//             foldedtoturncontinuationbet,
//             calledturncontinuationbet,
//             raisedturncontinuationbet,
//             facingrivercontinuationbet,
//             foldedtorivercontinuationbet,
//             calledrivercontinuationbet,
//             raisedrivercontinuationbet,
//             totalpostflopstreetsseen,
//             totalaggressivepostflopstreetsseen)
//         VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10,
//             $11, $12, $13, $14, $15, $16, $17, $18, $19, $20,
//             $21, $22, $23, $24, $25, $26, $27, $28, $29, $30,
//             $31, $32, $33, $34, $35, $36, $37, $38, $39, $40,
//             $41, $42, $43, $44, $45, $46, $47, $48, $49, $50,
//             $51, $52, $53, $54, $55, $56, $57, $58, $59, $60,
//             $61, $62, $63, $64, $65, $66, $67, $68, $69, $70,
//             $71, $72, $73, $74, $75, $76, $77, $78)
//         `, [
//             result.player_id, result.tablesize,
//             result.bigblindincents, result.playedyearandmonth,
//             result.currencytype_id, result.pokergametype_id,
//             result.totalhands,
//             result.totalamountwonincents,
//             result.totalrakeincents,
//             result.totalbbswon,
//             result.vpiphands,
//             result.pfrhands,
//             result.couldcoldcall,
//             result.didcoldcall,
//             result.couldthreebet,
//             result.didthreebet,
//             result.couldsqueeze,
//             result.didsqueeze,
//             result.facingtwopreflopraisers,
//             result.calledtwopreflopraisers,
//             result.raisedtwopreflopraisers,
//             result.smallblindstealattempted,
//             result.smallblindstealdefended,
//             result.smallblindstealreraised,
//             result.bigblindstealattempted,
//             result.bigblindstealdefended,
//             result.bigblindstealreraised,
//             result.sawnonsmallshowdown,
//             result.wonnonsmallshowdown,
//             result.sawlargeshowdown,
//             result.wonlargeshowdown,
//             result.sawnonsmallshowdownlimpedflop,
//             result.wonnonsmallshowdownlimpedflop,
//             result.sawlargeshowdownlimpedflop,
//             result.wonlargeshowdownlimpedflop,
//             result.wonhand,
//             result.wonhandwhensawflop,
//             result.wonhandwhensawturn,
//             result.wonhandwhensawriver,
//             result.facedthreebetpreflop,
//             result.foldedtothreebetpreflop,
//             result.calledthreebetpreflop,
//             result.raisedthreebetpreflop,
//             result.facedfourbetpreflop,
//             result.foldedtofourbetpreflop,
//             result.calledfourbetpreflop,
//             result.raisedfourbetpreflop,
//             result.turnfoldippassonflopcb,
//             result.turncallippassonflopcb,
//             result.turnraiseippassonflopcb,
//             result.riverfoldippassonturncb,
//             result.rivercallippassonturncb,
//             result.riverraiseippassonturncb,
//             result.sawflop,
//             result.sawshowdown,
//             result.wonshowdown,
//             result.totalbets,
//             result.totalcalls,
//             result.flopcontinuationbetpossible,
//             result.flopcontinuationbetmade,
//             result.turncontinuationbetpossible,
//             result.turncontinuationbetmade,
//             result.rivercontinuationbetpossible,
//             result.rivercontinuationbetmade,
//             result.facingflopcontinuationbet,
//             result.foldedtoflopcontinuationbet,
//             result.calledflopcontinuationbet,
//             result.raisedflopcontinuationbet,
//             result.facingturncontinuationbet,
//             result.foldedtoturncontinuationbet,
//             result.calledturncontinuationbet,
//             result.raisedturncontinuationbet,
//             result.facingrivercontinuationbet,
//             result.foldedtorivercontinuationbet,
//             result.calledrivercontinuationbet,
//             result.raisedrivercontinuationbet,
//             result.totalpostflopstreetsseen,
//             result.totalaggressivepostflopstreetsseen,
//         ]);
// }

// async function insertCompiledPlayerResults() {
//     const result = await work_db.one('select count(*) from mycompiledplayerresults');
//     const reqCount = Math.floor(result.count / 100) + 1;
//     for (let i = 0; i < reqCount; i += 1) {
//         const limit = 100;
//         const offset = limit * i;
//         const compiledResults = await work_db.many('select * from mycompiledplayerresults limit $1 offset $2', [limit, offset]);
//         for (const compiledResult of compiledResults) {
//             await insertOneCompiledPlayerResult(compiledResult);
//         }
//     }
//     return Promise.resolve();
// }

// function insertOneFormatedPlayerResult(playerResult) {
//     return end_point_db.any(`
//     INSERT INTO ${process.env.FORMATED_PLAYER_RESULTS_TABLE_NAME}
//     (player_id, result, hm_format_date, currencytype_id, pokergametype_id, pokersite_id, bigblindincents, tablesize)
//         VALUES ($1, $2, $3,$4, $5, $6,$7, $8)
//         `, [
//             playerResult.player_id,
//             playerResult.result,
//             playerResult.hm_format_date,
//             playerResult.currencytype_id,
//             playerResult.pokergametype_id,
//             playerResult.pokersite_id,
//             playerResult.bigblindincents,
//             playerResult.tablesize,
//         ]);
// }

// async function insertFormatedPlayerResults() {
//     const result = await work_db.one('select count(*) from formatedplayerresults');
//     const reqCount = Math.floor(result.count / 1000) + 1;
//     for (let i = 0; i < reqCount; i += 1) {
//         const limit = 1000;
//         const offset = limit * i;
//         const playerResults =
//             await work_db.many('select player_id, result, hm_format_date, currencytype_id, pokergametype_id, pokersite_id, bigblindincents, tablesize from formatedplayerresults limit $1 offset $2', [limit, offset]);
//         for (const playerResult of playerResults) {
//             await insertOneFormatedPlayerResult(playerResult);
//         }
//     }
//     return Promise.resolve();
// }

// end_point_db.connect()
//     .then(() => {
//         logger.info(`${process.env.DB_HM2_DB_NAME}: connection successful`);
//         return work_db.connect();
//     })
//     .then(() => {
//         logger.info(`${process.env.DB_WORK_DB_NAME}: connection successful`);
//         return players_db.connect();
//     })
//     .then(() => end_point_db.any(`
//         create table if not exists users(
//             id serial primary key,
//             email text,
//             password text,
//             salt text,
//             auth_secret text,
//             is_registered boolean,
//             is_admin boolean,
//             expiration_subscription_date bigint,
//             number_of_searches integer,
//             subscription_type integer,
//             last_token text
//         )
//     `))
//     .then(() => end_point_db.any(`
//         create table if not exists registration_tokens(
//             token text primary key,
//             status text,
//             user_id integer
//         )
//     `))
//     .then(() => end_point_db.any(`
//         CREATE TABLE if not exists players
//         (
//             player_id serial,
//             playername text,
//             pokersite_id smallint
//         )
//     `))
//     .then(() => {
//         logger.info(`${process.env.DB_PLAYERS_DB_NAME}: connection successful`);
//         return players_db.many('select * from players order by player_id');
//     })
//     .then((playersFromDb) => {
//         logger.info(`${process.env.DB_HM2_DB_NAME}: users get successful`);
//         const promises = [];
//         for (const player of playersFromDb) {
//             promises.push(insertNewPlayersToGlobalPlayersDB(player));
//         }

//         return Promise.all(promises);
//     })
//     .then(() => end_point_db.any(`DROP table IF EXISTS ${process.env.COMPILED_RESULTS_TABLE_NAME}`))
//     .then(() => {
//         logger.info(`${process.env.DB_WORK_DB_NAME}: drop ${process.env.COMPILED_RESULTS_TABLE_NAME} successful`);
//         return end_point_db.any(`create table ${process.env.COMPILED_RESULTS_TABLE_NAME}(
//             player_id integer,

//             tablesize smallint,
//             bigblindincents integer,
//             playedyearandmonth integer,
//             currencytype_id smallint,
//             pokergametype_id smallint,

//             totalhands integer NOT NULL,
//             totalamountwonincents integer NOT NULL,
//             totalrakeincents integer NOT NULL,
//             totalbbswon integer NOT NULL,
//             vpiphands integer NOT NULL,
//             pfrhands integer NOT NULL,
//             couldcoldcall integer NOT NULL,
//             didcoldcall integer NOT NULL,
//             couldthreebet integer NOT NULL,
//             didthreebet integer NOT NULL,
//             couldsqueeze integer NOT NULL,
//             didsqueeze integer NOT NULL,
//             facingtwopreflopraisers integer NOT NULL,
//             calledtwopreflopraisers integer NOT NULL,
//             raisedtwopreflopraisers integer NOT NULL,
//             smallblindstealattempted integer NOT NULL,
//             smallblindstealdefended integer NOT NULL,
//             smallblindstealreraised integer NOT NULL,
//             bigblindstealattempted integer NOT NULL,
//             bigblindstealdefended integer NOT NULL,
//             bigblindstealreraised integer NOT NULL,
//             sawnonsmallshowdown integer NOT NULL,
//             wonnonsmallshowdown integer NOT NULL,
//             sawlargeshowdown integer NOT NULL,
//             wonlargeshowdown integer NOT NULL,
//             sawnonsmallshowdownlimpedflop integer NOT NULL,
//             wonnonsmallshowdownlimpedflop integer NOT NULL,
//             sawlargeshowdownlimpedflop integer NOT NULL,
//             wonlargeshowdownlimpedflop integer NOT NULL,
//             wonhand integer NOT NULL,
//             wonhandwhensawflop integer NOT NULL,
//             wonhandwhensawturn integer NOT NULL,
//             wonhandwhensawriver integer NOT NULL,
//             facedthreebetpreflop integer NOT NULL,
//             foldedtothreebetpreflop integer NOT NULL,
//             calledthreebetpreflop integer NOT NULL,
//             raisedthreebetpreflop integer NOT NULL,
//             facedfourbetpreflop integer NOT NULL,
//             foldedtofourbetpreflop integer NOT NULL,
//             calledfourbetpreflop integer NOT NULL,
//             raisedfourbetpreflop integer NOT NULL,
//             turnfoldippassonflopcb integer NOT NULL,
//             turncallippassonflopcb integer NOT NULL,
//             turnraiseippassonflopcb integer NOT NULL,
//             riverfoldippassonturncb integer NOT NULL,
//             rivercallippassonturncb integer NOT NULL,
//             riverraiseippassonturncb integer NOT NULL,
//             sawflop integer NOT NULL,
//             sawshowdown integer NOT NULL,
//             wonshowdown integer NOT NULL,
//             totalbets integer NOT NULL,
//             totalcalls integer NOT NULL,
//             flopcontinuationbetpossible integer NOT NULL,
//             flopcontinuationbetmade integer NOT NULL,
//             turncontinuationbetpossible integer NOT NULL,
//             turncontinuationbetmade integer NOT NULL,
//             rivercontinuationbetpossible integer NOT NULL,
//             rivercontinuationbetmade integer NOT NULL,
//             facingflopcontinuationbet integer NOT NULL,
//             foldedtoflopcontinuationbet integer NOT NULL,
//             calledflopcontinuationbet integer NOT NULL,
//             raisedflopcontinuationbet integer NOT NULL,
//             facingturncontinuationbet integer NOT NULL,
//             foldedtoturncontinuationbet integer NOT NULL,
//             calledturncontinuationbet integer NOT NULL,
//             raisedturncontinuationbet integer NOT NULL,
//             facingrivercontinuationbet integer NOT NULL,
//             foldedtorivercontinuationbet integer NOT NULL,
//             calledrivercontinuationbet integer NOT NULL,
//             raisedrivercontinuationbet integer NOT NULL,
//             totalpostflopstreetsseen integer NOT NULL,
//             totalaggressivepostflopstreetsseen integer NOT NULL,

//             PRIMARY KEY (player_id, tablesize, bigblindincents, playedyearandmonth, currencytype_id, pokergametype_id)
//         )`);
//     })
//     .then(() => end_point_db.any(`DROP table IF EXISTS ${process.env.FORMATED_PLAYER_RESULTS_TABLE_NAME}`))
//     .then(() => end_point_db.any(`create table IF NOT EXISTS ${process.env.FORMATED_PLAYER_RESULTS_TABLE_NAME}(
//         id serial,
//         player_id integer,
//         result double precision,
//         hm_format_date bigint,
//         currencytype_id smallint,
//         pokergametype_id smallint,
//         pokersite_id smallint,
//         bigblindincents integer,
//         tablesize smallint
//         )`))
//     .then(() => insertCompiledPlayerResults())
//     .then(() => insertFormatedPlayerResults())
//     .then(() => {
//         process.exit();
//     });


require('dotenv').config();

const pgp = require('pg-promise')({});

const log4js = require('log4js');

log4js.configure({
    appenders: { DB: { type: 'file', filename: 'log.log' } },
    categories: { default: { appenders: ['DB'], level: 'trace' } },
});

const logger = log4js.getLogger('DB');

const end_point_cn = {
    host: process.env.DB_HOST,
    port: process.env.DB_PORT,
    database: process.env.DB_END_POINT_DB_NAME,
    user: process.env.DB_USER,
    password: process.env.DB_PASS,
};

const work_cn = {
    host: process.env.DB_HOST,
    port: process.env.DB_PORT,
    database: process.env.DB_WORK_DB_NAME,
    user: process.env.DB_USER,
    password: process.env.DB_PASS,
};

const compiledResultsTableName = process.argv[2] || process.env.COMPILED_RESULTS_TABLE_NAME;
const formatedPlayerResultsTableName = process.argv[3] || process.env.FORMATED_PLAYER_RESULTS_TABLE_NAME;

const end_point_db = pgp(end_point_cn);
const work_db = pgp(work_cn);

function insertOneCompiledPlayerResult(result) {
    return end_point_db.any(`
    INSERT INTO ${compiledResultsTableName}
        (playername, pokersite_id, tablesize, bigblindincents, playedyearandmonth, currencytype_id, pokergametype_id,
            totalhands,
            totalamountwonincents,
            totalrakeincents,
            totalbbswon,
            vpiphands,
            pfrhands,
            couldcoldcall,
            didcoldcall,
            couldthreebet,
            didthreebet,
            couldsqueeze,
            didsqueeze,
            facingtwopreflopraisers,
            calledtwopreflopraisers,
            raisedtwopreflopraisers,
            smallblindstealattempted,
            smallblindstealdefended,
            smallblindstealreraised,
            bigblindstealattempted,
            bigblindstealdefended,
            bigblindstealreraised,
            sawnonsmallshowdown,
            wonnonsmallshowdown,
            sawlargeshowdown,
            wonlargeshowdown,
            sawnonsmallshowdownlimpedflop,
            wonnonsmallshowdownlimpedflop,
            sawlargeshowdownlimpedflop,
            wonlargeshowdownlimpedflop,
            wonhand,
            wonhandwhensawflop,
            wonhandwhensawturn,
            wonhandwhensawriver,
            facedthreebetpreflop,
            foldedtothreebetpreflop,
            calledthreebetpreflop,
            raisedthreebetpreflop,
            facedfourbetpreflop,
            foldedtofourbetpreflop,
            calledfourbetpreflop,
            raisedfourbetpreflop,
            turnfoldippassonflopcb,
            turncallippassonflopcb,
            turnraiseippassonflopcb,
            riverfoldippassonturncb,
            rivercallippassonturncb,
            riverraiseippassonturncb,
            sawflop,
            sawshowdown,
            wonshowdown,
            totalbets,
            totalcalls,
            flopcontinuationbetpossible,
            flopcontinuationbetmade,
            turncontinuationbetpossible,
            turncontinuationbetmade,
            rivercontinuationbetpossible,
            rivercontinuationbetmade,
            facingflopcontinuationbet,
            foldedtoflopcontinuationbet,
            calledflopcontinuationbet,
            raisedflopcontinuationbet,
            facingturncontinuationbet,
            foldedtoturncontinuationbet,
            calledturncontinuationbet,
            raisedturncontinuationbet,
            facingrivercontinuationbet,
            foldedtorivercontinuationbet,
            calledrivercontinuationbet,
            raisedrivercontinuationbet,
            totalpostflopstreetsseen,
            totalaggressivepostflopstreetsseen)
        VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10,
            $11, $12, $13, $14, $15, $16, $17, $18, $19, $20,
            $21, $22, $23, $24, $25, $26, $27, $28, $29, $30,
            $31, $32, $33, $34, $35, $36, $37, $38, $39, $40,
            $41, $42, $43, $44, $45, $46, $47, $48, $49, $50,
            $51, $52, $53, $54, $55, $56, $57, $58, $59, $60,
            $61, $62, $63, $64, $65, $66, $67, $68, $69, $70,
            $71, $72, $73, $74, $75, $76, $77, $78, $79)
        `, [
            result.playername, result.pokersite_id, result.tablesize,
            result.bigblindincents, result.playedyearandmonth,
            result.currencytype_id, result.pokergametype_id,
            result.totalhands,
            result.totalamountwonincents,
            result.totalrakeincents,
            result.totalbbswon,
            result.vpiphands,
            result.pfrhands,
            result.couldcoldcall,
            result.didcoldcall,
            result.couldthreebet,
            result.didthreebet,
            result.couldsqueeze,
            result.didsqueeze,
            result.facingtwopreflopraisers,
            result.calledtwopreflopraisers,
            result.raisedtwopreflopraisers,
            result.smallblindstealattempted,
            result.smallblindstealdefended,
            result.smallblindstealreraised,
            result.bigblindstealattempted,
            result.bigblindstealdefended,
            result.bigblindstealreraised,
            result.sawnonsmallshowdown,
            result.wonnonsmallshowdown,
            result.sawlargeshowdown,
            result.wonlargeshowdown,
            result.sawnonsmallshowdownlimpedflop,
            result.wonnonsmallshowdownlimpedflop,
            result.sawlargeshowdownlimpedflop,
            result.wonlargeshowdownlimpedflop,
            result.wonhand,
            result.wonhandwhensawflop,
            result.wonhandwhensawturn,
            result.wonhandwhensawriver,
            result.facedthreebetpreflop,
            result.foldedtothreebetpreflop,
            result.calledthreebetpreflop,
            result.raisedthreebetpreflop,
            result.facedfourbetpreflop,
            result.foldedtofourbetpreflop,
            result.calledfourbetpreflop,
            result.raisedfourbetpreflop,
            result.turnfoldippassonflopcb,
            result.turncallippassonflopcb,
            result.turnraiseippassonflopcb,
            result.riverfoldippassonturncb,
            result.rivercallippassonturncb,
            result.riverraiseippassonturncb,
            result.sawflop,
            result.sawshowdown,
            result.wonshowdown,
            result.totalbets,
            result.totalcalls,
            result.flopcontinuationbetpossible,
            result.flopcontinuationbetmade,
            result.turncontinuationbetpossible,
            result.turncontinuationbetmade,
            result.rivercontinuationbetpossible,
            result.rivercontinuationbetmade,
            result.facingflopcontinuationbet,
            result.foldedtoflopcontinuationbet,
            result.calledflopcontinuationbet,
            result.raisedflopcontinuationbet,
            result.facingturncontinuationbet,
            result.foldedtoturncontinuationbet,
            result.calledturncontinuationbet,
            result.raisedturncontinuationbet,
            result.facingrivercontinuationbet,
            result.foldedtorivercontinuationbet,
            result.calledrivercontinuationbet,
            result.raisedrivercontinuationbet,
            result.totalpostflopstreetsseen,
            result.totalaggressivepostflopstreetsseen,
        ]);
}

async function insertCompiledPlayerResults() {
    const result = await work_db.one('select count(*) from mycompiledplayerresults');
    const reqCount = Math.floor(result.count / 5000) + 1;
    for (let i = 0; i < reqCount; i += 1) {
        console.log(`insert compiled ${i * 5000} / ${result.count}`);
        const limit = 5000;
        const offset = limit * i;
        const compiledResults = await work_db.many('select * from mycompiledplayerresults order by playername, pokersite_id, tablesize,bigblindincents, playedyearandmonth, currencytype_id limit $1 offset $2', [limit, offset]);
        for (const compiledResult of compiledResults) {
            await insertOneCompiledPlayerResult(compiledResult);
        }
    }
    return Promise.resolve();
}

function insertOneFormatedPlayerResult(playerResult) {
    return end_point_db.any(`
    INSERT INTO ${formatedPlayerResultsTableName}
    (playername, result, hm_format_date, currencytype_id, pokergametype_id, pokersite_id, bigblindincents, tablesize, hand_id)
        VALUES ($1, $2, $3,$4, $5, $6,$7, $8, $9)
        `, [
            playerResult.playername,
            playerResult.result,
            playerResult.hm_format_date,
            playerResult.currencytype_id,
            playerResult.pokergametype_id,
            playerResult.pokersite_id,
            playerResult.bigblindincents,
            playerResult.tablesize,
            playerResult.hand_id,
        ])
        .catch((err) => {
            console.log('formated err', err);
        });
}

// async function insertFormatedPlayerResults() {
//     const result = await work_db.one('select count(*) from formatedplayerresults');
//     const reqCount = Math.floor(result.count / 1000) + 1;
//     for (let i = 0; i < reqCount; i += 1) {
//         console.log(`insert formated ${i * 1000} / ${result.count}`);
//         const limit = 1000;
//         const offset = limit * i;
//         const playerResults =
//             await work_db.many('select playername, result, hm_format_date, currencytype_id, pokergametype_id, pokersite_id, bigblindincents, tablesize, hand_id from formatedplayerresults limit $1 offset $2', [limit, offset]);
//         for (const playerResult of playerResults) {
//             await insertOneFormatedPlayerResult(playerResult);
//         }
//     }
//     return Promise.resolve();
// }

async function insertFormatedPlayerResults() {
    // const players = await work_db.many('select playername from formatedplayerresults group by playername order by playername');

    // for (let i = 0; i < players.length; i += 1) {
    //     console.log(`insert formated ${i} / ${players.length}`);

    //     const playerResults = await work_db.many(`
    //         select playername, result, hm_format_date, currencytype_id, pokergametype_id, pokersite_id, bigblindincents, tablesize, hand_id
    //         from formatedplayerresults where playername=$1
    //     `, [players[i].playername]);
    //     for (const playerResult of playerResults) {
    //         await insertOneFormatedPlayerResult(playerResult);
    //     }
    // }


    const result = await work_db.one('select count(*) from formatedplayerresults');
    const reqCount = Math.floor(result.count / 1000) + 1;
    for (let i = 0; i < reqCount; i += 1) {
        console.log(`insert formated ${i * 1000} / ${result.count}`);
        const limit = 1000;
        const offset = limit * i;
        const playerResults =
            await work_db.many(`
            select playername, result, hm_format_date, currencytype_id, pokergametype_id, pokersite_id, bigblindincents, tablesize, hand_id from formatedplayerresults
            order by id limit $1 offset $2`, [limit, offset]);
        for (const playerResult of playerResults) {
            await insertOneFormatedPlayerResult(playerResult);
        }
    }
    return Promise.resolve();
}

async function generatePlayerNames() {
    console.log('insert recalculate playernames');
    // await end_point_db.any('drop table if exists players');
    await end_point_db.any('create table if not exists players(id serial primary key,playername text, normalized_playername text, pokersite_id smallint, home_limit integer)');

    // const tableNames = await end_point_db.many('SELECT table_name FROM information_schema.tables WHERE table_schema=\'public\' AND table_type=\'BASE TABLE\' AND table_name like \'mycompiledplayerresults%\'');
    // let queryToDb = '';
    // if (tableNames && tableNames.length > 0) {
    //     for (let i = 0; i < tableNames.length; i += 1) {
    //         const query = `select playername, Lower(playername) as normalized_playername, pokersite_id from ${tableNames[i].table_name} group by playername,normalized_playername, pokersite_id`;
    //         if (i + 1 !== tableNames.length) {
    //             queryToDb = `${queryToDb} ${query} union`;
    //         } else {
    //             queryToDb = `${queryToDb} ${query}`;
    //         }
    //     }
    // }

    // const players = await end_point_db.any(`
    // select * from (${queryToDb}) as a
    // `);

    // for (let i = 0; i < players.length; i += 1) {
    //     console.log('insert ', players[i].playername, i, ' / ', players.length);
    //     await end_point_db.one(`
    //         select * from players where playername = $1
    //     `, [players[i].playername])
    //         .catch(async () => {
    //             await end_point_db.any(`
    //             insert into players (playername,normalized_playername, pokersite_id, home_limit) values ($1, $2, $3, $4)
    //         `, [players[i].playername, players[i].normalized_playername, players[i].pokersite_id, null]);
    //         });
    // }

    const players = await work_db.many('select playername, pokersite_id from mycompiledplayerresults group by playername, pokersite_id');

    for (let i = 0; i < players.length; i += 1) {
        console.log('insert players', players[i].playername, i, ' / ', players.length);
        await end_point_db.one(`
            select * from players where playername = $1 and pokersite_id = $2
        `, [players[i].playername, players[i].pokersite_id])
            .catch(async () => {
                console.log('NEW');
                await end_point_db.any(`
                insert into players (playername,normalized_playername, pokersite_id, home_limit) values ($1, $2, $3, $4)
            `, [players[i].playername, players[i].playername.toLowerCase(), players[i].pokersite_id, null]);
            });
    }

    // await end_point_db.any(`
    // insert into players (playername,normalized_playername, pokersite_id ) (
    //     select * from (
    //         ${queryToDb}
    //     ) as a group by playername,normalized_playername, pokersite_id)
    // `);
}

async function convertToNormalizedPlayerNames() {
    // const formated = await end_point_db.many('SELECT table_name FROM information_schema.tables WHERE table_schema=\'public\' AND table_type=\'BASE TABLE\' AND table_name like \'formatedplayerresults%\'');
    // const compiled = await end_point_db.many('SELECT table_name FROM information_schema.tables WHERE table_schema=\'public\' AND table_type=\'BASE TABLE\' AND table_name like \'mycompiledplayerresults%\'');
    await end_point_db.any(`alter table ${process.env.COMPILED_RESULTS_TABLE_NAME} drop column if exists normalized_playername `);
    await end_point_db.any(`alter table ${process.env.COMPILED_RESULTS_TABLE_NAME} add column normalized_playername text `);

    await end_point_db.any(`alter table ${process.env.FORMATED_PLAYER_RESULTS_TABLE_NAME} drop column if exists normalized_playername `);
    await end_point_db.any(`alter table ${process.env.FORMATED_PLAYER_RESULTS_TABLE_NAME} add column normalized_playername text `);

    await end_point_db.any(`update ${process.env.COMPILED_RESULTS_TABLE_NAME} set normalized_playername = lower(playername)`);
    await end_point_db.any(`update ${process.env.FORMATED_PLAYER_RESULTS_TABLE_NAME} set normalized_playername = lower(playername)`);

    // for (const i of formated.concat(compiled)) {
    //     console.log('start normalizing ', i.table_name);
    //     await end_point_db.any(`update ${i.table_name} set normalized_playername = lower(playername)`);
    //     // await end_point_db.any(`alter table ${i.table_name} drop column normalized_playername `);
    //     // await end_point_db.any(`alter table ${i.table_name} add column normalized_playername text `)
    //     //     .then(() => {
    //     //         console.log('normalized succes ', i.table_name);
    //     //         return end_point_db.any(`update ${i.table_name} set normalized_playername = lower(playername)`);
    //     //     }).catch(() => {
    //     //         console.log('already normalized ', i.table_name);
    //     //         return Promise.resolve();
    //     //     });
    // }

    return Promise.resolve();
}

const nicksToTest = ['fobot21', 'FuzzF@ce', 'app1e_j@ck'];

async function createNormilizedGraph() {
    const compiled = await end_point_db.many('SELECT table_name FROM information_schema.tables WHERE table_schema=\'public\' AND table_type=\'BASE TABLE\' AND table_name like \'mycompiledplayerresults%\'');

    const compiledColumns = await end_point_db.many(
        'select column_name,data_type from information_schema.columns where table_name = $1',
        [compiled[0].table_name],
    );

    let columnsToCreate = '(';
    for (let i = 0; i < compiledColumns.length; i += 1) {
        if (i !== compiledColumns.length - 1) {
            columnsToCreate = `${columnsToCreate} ${compiledColumns[i].column_name} ${compiledColumns[i].data_type}, `;
        } else {
            columnsToCreate = `${columnsToCreate} ${compiledColumns[i].column_name} ${compiledColumns[i].data_type}`;
        }
    }

    columnsToCreate = `${columnsToCreate}) `;

    const preFormatedTableName = `pre_formatedplayerresults_${process.env.COMPILED_RESULTS_TABLE_NAME.replace('mycompiledplayerresults_', '')}`;

    await end_point_db.any(`drop table if exists pre_${process.env.COMPILED_RESULTS_TABLE_NAME}`);
    await end_point_db.any(`create table pre_${process.env.COMPILED_RESULTS_TABLE_NAME} (id serial primary key, playername text, pokersite_id smallint, stats text)`);

    await end_point_db.any(`drop table if exists ${preFormatedTableName}`);
    await end_point_db.any(`create table ${preFormatedTableName} (id serial primary key, num integer, result double precision, playername text, hm_format_date integer, pokersite_id smallint )`);
    // await end_point_db.any(`create table ${preFormatedTableName} (id serial primary key, result text, playername text, hm_format_date integer, pokersite_id smallint )`);

    let queryToCompiled = 'sum(winnings) as winnings,';
    for (let i = 0; i < compiledColumns.length; i += 1) {
        if (compiledColumns[i].data_type === 'integer' && compiledColumns[i].column_name !== 'bigblindincents' && compiledColumns[i].column_name !== 'playedyearandmonth') {
            if (i !== compiledColumns.length - 2) {
                queryToCompiled = `${queryToCompiled} SUM(${compiledColumns[i].column_name}) as ${compiledColumns[i].column_name}, `;
            } else {
                queryToCompiled = `${queryToCompiled} SUM(${compiledColumns[i].column_name}) as ${compiledColumns[i].column_name}`;
            }
        } else if (compiledColumns[i].column_name === 'playedyearandmonth') {
            queryToCompiled = `${queryToCompiled} MIN(${compiledColumns[i].column_name}) as ${compiledColumns[i].column_name}, `;
        }
    }

    const players = await end_point_db.many(`select playername,pokersite_id from ${process.env.COMPILED_RESULTS_TABLE_NAME} group by playername, pokersite_id order by playername`);
    const playersCount = players.length;


    for (let i = 0; i < playersCount; i += 1) {
        // if (i < 480012) {
        //     continue;
        // }
        // if (players[i].playername !== 'fobot21') {
        //     continue;
        // }
        // if (i < 21572) {
        //     continue;
        // }
        console.log('insert ', players[i].playername, i, ' / ', playersCount);

        const r = await end_point_db.one(`SELECT ${queryToCompiled} from (
            select *, ((select cast((totalamountwonincents) as float)) / 100 * currencies.value)::numeric(10,2) as winnings from ${process.env.COMPILED_RESULTS_TABLE_NAME}
                join currencies on ${process.env.COMPILED_RESULTS_TABLE_NAME}.currencytype_id = currencies.currency_id
                and ${process.env.COMPILED_RESULTS_TABLE_NAME}.playedyearandmonth = currencies.hm_format_date
                where playername=$1 and pokersite_id=$2
        ) as a group by playername`, [players[i].playername, players[i].pokersite_id])
            .catch((err) => {
                console.log(err);
            });

        if (!r) {
            continue;
        }

        await end_point_db.any(
            `INSERT INTO pre_${process.env.COMPILED_RESULTS_TABLE_NAME} (playername, pokersite_id, stats) values ($1, $2, $3)`,
            [players[i].playername, players[i].pokersite_id, JSON.stringify(r)],
        );

        // const monthResult = await end_point_db.one(`
        //             select playername, MIN(playedyearandmonth) as playedyearandmonth,
        //             ((select cast(sum(totalbbswon) as float))/nullif((select cast(sum(totalhands) as float) ),0))::numeric(7,1) as winrate,
        //             sum(totalhands) as totalhands,
        //             sum(vpiphands) as vpiphands,
        //             sum(pfrhands) as pfrhands,
        //             sum(didthreebet) as didthreebet,
        //             sum(couldthreebet) as couldthreebet,
        //             sum(winnings) as winnings,
        //             sum(totalbbswon) as totalbbswon,

        //             sum(flopcontinuationbetmade) as flopcontinuationbetmade,
        //             sum(turncontinuationbetmade) as turncontinuationbetmade,
        //             sum(rivercontinuationbetmade) as rivercontinuationbetmade,
        //             sum(flopcontinuationbetpossible) as flopcontinuationbetpossible,
        //             sum(turncontinuationbetpossible) as turncontinuationbetpossible,
        //             sum(rivercontinuationbetpossible) as rivercontinuationbetpossible,

        //             sum(foldedtoflopcontinuationbet) as foldedtoflopcontinuationbet,
        //             sum(foldedtoturncontinuationbet) as foldedtoturncontinuationbet,
        //             sum(foldedtorivercontinuationbet) as foldedtorivercontinuationbet,
        //             sum(facingflopcontinuationbet) as facingflopcontinuationbet,
        //             sum(facingturncontinuationbet) as facingturncontinuationbet,
        //             sum(facingrivercontinuationbet) as facingrivercontinuationbet,

        //             sum(wonhandwhensawflop) as wonhandwhensawflop,
        //             sum(sawflop) as sawflop,
        //             sum(sawshowdown) as sawshowdown,
        //             sum(wonshowdown) as wonshowdown
        //             from(
        //                 select *, ((select cast((totalamountwonincents) as float)) / 100 * currencies.value)::numeric(10,2) as winnings from ${process.env.COMPILED_RESULTS_TABLE_NAME}
        //                     join currencies on ${process.env.COMPILED_RESULTS_TABLE_NAME}.currencytype_id = currencies.currency_id
        //                     and ${process.env.COMPILED_RESULTS_TABLE_NAME}.playedyearandmonth = currencies.hm_format_date
        //                     where playername=$1 and pokersite_id=$2
        //             ) as a group by playername
        //         `, [players[i].playername, players[i].pokersite_id])
        //     .catch(() => {
        //         // console.log(err);
        //     });

        // const str = JSON.stringify(monthResult);
        // console.log(`pre_${compiledTable.table_name}`);
        // await end_point_db.any(`INSERT INTO pre_${compiledTable.table_name} (playername, stats, pokersite_id) VALUES ($1,$2, $3)`, [players[i].playername, str, players[i].pokersite_id]);

        // const json = JSON.parse(str);
        // console.log(str, json);

        // const hands = +r.totalhands;
        // let blockSize = Math.floor(hands / 1000);
        // if (blockSize === 0) {
        //     blockSize = 1;
        // }

        // let result = await end_point_db.many(
        //     `
        //                     select cast (rnum / $3 as int) as num, sum(result) as result, MIN(hm_format_date) as hm_format_date from (
        //                         select row_number() OVER () as rnum, result, hm_format_date from (
        //                         select result, hm_format_date from (
        //                             select hand_id, result, hm_format_date from ${process.env.FORMATED_PLAYER_RESULTS_TABLE_NAME} where playername = $1 and pokersite_id = $2
        //                     ) as a order by hand_id) as b) as c group by num
        //                 `
        //     , [players[i].playername, players[i].pokersite_id, blockSize],
        // ).catch(() => {

        // });


        // if (!result) {
        //     const pointsCount = +r.totalhands / 10 >= 1 ? 10 : +r.totalhands;
        //     const min = -100;
        //     const max = 100;
        //     let sum = 0;
        //     const gr = [];
        //     for (let m = 0; m < pointsCount; m += 1) {
        //         const k = Math.round(-0.5 + Math.random() * (100 + 1));
        //         const ra = Math.round(min - 0.5 + Math.random() * (max - min + 1)) * k;
        //         sum += ra;
        //         gr.push(ra);
        //     }

        //     const pointWeight = sum !== 0 ? +r.winnings / sum : 1;
        //     // if (pointWeight === -Infinity || pointWeight === Infinity || isNaN(pointWeight)) {
        //     //     console.log('err');
        //     // }

        //     result = gr.map((x, index) => ({
        //         num: index + 1,
        //         result: x * pointWeight,
        //         hm_format_date: r.playedyearandmonth,
        //     }));
        // } else {
        //     let sum = 0;
        //     for (const res of result) {
        //         sum += +res.result;
        //     }
        //     const diff = (+r.winnings - sum) / result.length;
        //     for (const res of result) {
        //         res.result = +res.result + diff;
        //     }
        // }

        // // result = result.map(x => x.result);

        // // await end_point_db.any(`INSERT INTO ${preFormatedTableName} (result,playername,hm_format_date, pokersite_id) VALUES ($1, $2, $3,$4)
        // // `, [JSON.stringify(result), players[i].playername, monthResult.playedyearandmonth, players[i].pokersite_id]);

        // let q = `INSERT INTO ${preFormatedTableName} (num,result,playername,hm_format_date, pokersite_id) VALUES`;

        // const preparedName = pgp.as.format('$1', players[i].playername);


        // for (let m = 0; m < result.length; m += 1) {
        //     if (m === result.length - 1) {
        //         q = `${q} (${result[m].num}, ${result[m].result}, ${preparedName},${result[m].hm_format_date}, ${players[i].pokersite_id})`;
        //     } else {
        //         q = `${q} (${result[m].num}, ${result[m].result}, ${preparedName},${result[m].hm_format_date}, ${players[i].pokersite_id}),`;
        //     }
        // }

        // await end_point_db.any(q)
        //     .catch((e) => {
        //         console.log(e);
        //     });

        // const queries = result.map(res => end_point_db.any(`
        //     INSERT INTO ${preFormatedTableName} (num,result,playername,hm_format_date, pokersite_id) VALUES ($1, $2, $3,$4, $5)
        //     `, [res.num, res.result, players[i].playername, res.hm_format_date, players[i].pokersite_id]));

        // await Promise.all(queries);
    }
}

// async function createNormilizedGraph() {
//     const formated = await end_point_db.many('SELECT table_name FROM information_schema.tables WHERE table_schema=\'public\' AND table_type=\'BASE TABLE\' AND table_name like \'formatedplayerresults%\'');
//     const compiled = await end_point_db.many('SELECT table_name FROM information_schema.tables WHERE table_schema=\'public\' AND table_type=\'BASE TABLE\' AND table_name like \'mycompiledplayerresults%\'');

//     for (const tableName of compiled) {
//         // const newTableName = `pre_${tableName.table_name}`;
//         // await end_point_db.any(`drop table if exists ${newTableName}`);
//         // end_point_db.any(`create table ${newTableName} (id serial primary key, num integer, result double precision, playername text, hm_format_date integer, pokersite_id smallint )`);
//         const preFormatedTableName = `pre_formatedplayerresults_${tableName.table_name.replace('mycompiledplayerresults_', '')}`;
//         await end_point_db.any(`drop table if exists ${preFormatedTableName}`);
//         await end_point_db.any(`create table ${preFormatedTableName} (id serial primary key, result text, playername text, hm_format_date integer, pokersite_id smallint )`);
//     }

//     // const compiled = await end_point_db.many('SELECT table_name FROM information_schema.tables WHERE table_schema=\'public\' AND table_type=\'BASE TABLE\' AND table_name like \'mycompiledplayerresults%\'');

//     const compiledColumns = await end_point_db.many(
//         'select column_name,data_type from information_schema.columns where table_name = $1',
//         [compiled[0].table_name],
//     );

//     let columnsToCreate = '(';
//     for (let i = 0; i < compiledColumns.length; i += 1) {
//         if (i !== compiledColumns.length - 1) {
//             columnsToCreate = `${columnsToCreate} ${compiledColumns[i].column_name} ${compiledColumns[i].data_type}, `;
//         } else {
//             columnsToCreate = `${columnsToCreate} ${compiledColumns[i].column_name} ${compiledColumns[i].data_type}`;
//         }
//     }

//     columnsToCreate = `${columnsToCreate}) `;
//     let queryToCompiled = 'sum(winnings) as winnings,';
//     for (let i = 0; i < compiledColumns.length; i += 1) {
//         if (compiledColumns[i].data_type === 'integer' && compiledColumns[i].column_name !== 'bigblindincents' && compiledColumns[i].column_name !== 'playedyearandmonth') {
//             if (i !== compiledColumns.length - 2) {
//                 queryToCompiled = `${queryToCompiled} SUM(${compiledColumns[i].column_name}) as ${compiledColumns[i].column_name}, `;
//             } else {
//                 queryToCompiled = `${queryToCompiled} SUM(${compiledColumns[i].column_name}) as ${compiledColumns[i].column_name}`;
//             }
//         } else if (compiledColumns[i].column_name === 'playedyearandmonth') {
//             queryToCompiled = `${queryToCompiled} MIN(${compiledColumns[i].column_name}) as ${compiledColumns[i].column_name}, `;
//         }
//     }

//     const players = await end_point_db.many('select * from players');
//     const playersCount = players.length;


//     for (let i = 0; i < playersCount; i += 1) {
//         // if (i < 480012) {
//         //     continue;
//         // }
//         // if (players[i].playername !== 'fobot21') {
//         //     continue;
//         // }
//         console.log('insert ', players[i].playername, i, ' / ', playersCount);

//         for (const table of compiled) {
//             const tableName = table.table_name;
//             const preFormatedTableName = `pre_formatedplayerresults_${tableName.replace('mycompiledplayerresults_', '')}`;
//             const formatedTableName = `formatedplayerresults_${tableName.replace('mycompiledplayerresults_', '')}`;
//             const r = await end_point_db.one(`SELECT ${queryToCompiled} from (
//             select *, ((select cast((totalamountwonincents) as float)) / 100 * currencies.value)::numeric(10,2) as winnings from ${tableName}
//                 join currencies on ${tableName}.currencytype_id = currencies.currency_id
//                 and ${tableName}.playedyearandmonth = currencies.hm_format_date
//                 where playername=$1 and pokersite_id=$2
//         ) as a group by playername`, [players[i].playername, players[i].pokersite_id])
//                 .catch((err) => {
//                     // console.log(err);
//                 });

//             if (!r) {
//                 continue;
//             }

//             // await end_point_db.any(
//             //     `INSERT INTO pre_${process.env.COMPILED_RESULTS_TABLE_NAME} (playername, pokersite_id, stats) values ($1, $2, $3)`,
//             //     [players[i].playername, players[i].pokersite_id, JSON.stringify(r)],
//             // );

//             // const monthResult = await end_point_db.one(`
//             //         select playername, MIN(playedyearandmonth) as playedyearandmonth,
//             //         ((select cast(sum(totalbbswon) as float))/nullif((select cast(sum(totalhands) as float) ),0))::numeric(7,1) as winrate,
//             //         sum(totalhands) as totalhands,
//             //         sum(vpiphands) as vpiphands,
//             //         sum(pfrhands) as pfrhands,
//             //         sum(didthreebet) as didthreebet,
//             //         sum(couldthreebet) as couldthreebet,
//             //         sum(winnings) as winnings,
//             //         sum(totalbbswon) as totalbbswon,

//             //         sum(flopcontinuationbetmade) as flopcontinuationbetmade,
//             //         sum(turncontinuationbetmade) as turncontinuationbetmade,
//             //         sum(rivercontinuationbetmade) as rivercontinuationbetmade,
//             //         sum(flopcontinuationbetpossible) as flopcontinuationbetpossible,
//             //         sum(turncontinuationbetpossible) as turncontinuationbetpossible,
//             //         sum(rivercontinuationbetpossible) as rivercontinuationbetpossible,

//             //         sum(foldedtoflopcontinuationbet) as foldedtoflopcontinuationbet,
//             //         sum(foldedtoturncontinuationbet) as foldedtoturncontinuationbet,
//             //         sum(foldedtorivercontinuationbet) as foldedtorivercontinuationbet,
//             //         sum(facingflopcontinuationbet) as facingflopcontinuationbet,
//             //         sum(facingturncontinuationbet) as facingturncontinuationbet,
//             //         sum(facingrivercontinuationbet) as facingrivercontinuationbet,

//             //         sum(wonhandwhensawflop) as wonhandwhensawflop,
//             //         sum(sawflop) as sawflop,
//             //         sum(sawshowdown) as sawshowdown,
//             //         sum(wonshowdown) as wonshowdown
//             //         from(
//             //             select *, ((select cast((totalamountwonincents) as float)) / 100 * currencies.value)::numeric(10,2) as winnings from ${tableName}
//             //                 join currencies on ${tableName}.currencytype_id = currencies.currency_id
//             //                 and ${tableName}.playedyearandmonth = currencies.hm_format_date
//             //                 where playername=$1 and pokersite_id=$2
//             //         ) as a group by playername
//             //     `, [players[i].playername, players[i].pokersite_id])
//             //     .catch(() => {
//             //         // console.log(err);
//             //     });

//             // console.log(`pre_${compiledTable.table_name}`);
//             // await end_point_db.any(`INSERT INTO pre_${compiledTable.table_name} (playername, stats, pokersite_id) VALUES ($1,$2, $3)`, [players[i].playername, str, players[i].pokersite_id]);

//             // const json = JSON.parse(str);
//             // console.log(str, json);

//             const hands = +r.totalhands;
//             let blockSize = Math.floor(hands / 5000);
//             if (blockSize === 0) {
//                 blockSize = 1;
//             }

//             let result = await end_point_db.many(
//                 `
//                             select cast (rnum / $3 as int) as num, sum(result) as result, MIN(hm_format_date) as hm_format_date from (
//                                 select row_number() OVER () as rnum, result, hm_format_date from (
//                                 select result, hm_format_date from (
//                                     select hand_id, result, hm_format_date from ${formatedTableName} where playername = $1 and pokersite_id = $2
//                             ) as a order by hand_id) as b) as c group by num
//                         `
//                 , [players[i].playername, players[i].pokersite_id, blockSize],
//             ).catch(() => {

//             });

//             if (!result) {
//                 const pointsCount = +r.totalhands / 10 >= 1 ? 10 : +r.totalhands;
//                 const min = -100;
//                 const max = 100;
//                 let sum = 0;
//                 const gr = [];
//                 for (let m = 0; m < pointsCount; m += 1) {
//                     const k = Math.round(-0.5 + Math.random() * (100 + 1));
//                     const r1 = Math.round(min - 0.5 + Math.random() * (max - min + 1)) * k;
//                     sum += r1;
//                     gr.push(r1);
//                 }

//                 const pointWeight = +r.winnings / sum;
//                 result = gr.map((x, index) => ({
//                     num: index + 1,
//                     result: x * pointWeight,
//                     hm_format_date: r.playedyearandmonth,
//                 }));
//             } else {
//                 let sum = 0;
//                 for (const res of result) {
//                     sum += +res.result;
//                 }
//                 const diff = (+r.winnings - sum) / result.length;
//                 for (const res of result) {
//                     res.result = +res.result + diff;
//                 }
//             }

//             result = result.map(x => x.result);

//             await end_point_db.any(`INSERT INTO ${preFormatedTableName} (result,playername,hm_format_date, pokersite_id) VALUES ($1, $2, $3,$4)
//             `, [JSON.stringify(result), players[i].playername, r.playedyearandmonth, players[i].pokersite_id]);

//             // const queries = result.map(res => end_point_db.any(`
//             //     INSERT INTO ${preFormatedTableName} (num,result,playername,hm_format_date, pokersite_id) VALUES ($1, $2, $3,$4, $5)
//             //     `, [res.num, res.result, players[i].playername, res.hm_format_date, players[i].pokersite_id]));

//             // await Promise.all(queries);
//         }
//     }
// }

// async function createNormilizedGraph() {
//     const compiled = await end_point_db.many('SELECT table_name FROM information_schema.tables WHERE table_schema=\'public\' AND table_type=\'BASE TABLE\' AND table_name like \'mycompiledplayerresults%\'');
//     const compiledColumns = await end_point_db.many(
//         'select column_name,data_type from information_schema.columns where table_name = $1',
//         [compiled[0].table_name],
//     );

//     let columnsToCreate = '(';
//     for (let i = 0; i < compiledColumns.length; i += 1) {
//         if (i !== compiledColumns.length - 1) {
//             columnsToCreate = `${columnsToCreate} ${compiledColumns[i].column_name} ${compiledColumns[i].data_type}, `;
//         } else {
//             columnsToCreate = `${columnsToCreate} ${compiledColumns[i].column_name} ${compiledColumns[i].data_type}`;
//         }
//     }

//     columnsToCreate = `${columnsToCreate}) `;

//     for (const table of compiled) {
//         const tableIndex = compiled.indexOf(table);
//         const s = compiled.length;
//         const tableName = table.table_name;
//         const formatedTableName = `formatedplayerresults_${tableName.replace('mycompiledplayerresults_', '')}`;

//         const preFormatedTableName = `pre_formatedplayerresults_${tableName.replace('mycompiledplayerresults_', '')}`;

//         // await end_point_db.any(`drop table if exists pre_${process.env.COMPILED_RESULTS_TABLE_NAME}`);
//         // await end_point_db.any(`create table pre_${process.env.COMPILED_RESULTS_TABLE_NAME} (id serial primary key, playername text, pokersite_id smallint, stats text)`);

//         await end_point_db.any(`drop table if exists ${preFormatedTableName}`);
//         await end_point_db.any(`create table ${preFormatedTableName} (id serial primary key, num integer, result double precision, playername text, hm_format_date integer, pokersite_id smallint )`);
//         // await end_point_db.any(`create table ${preFormatedTableName} (id serial primary key, result text, playername text, hm_format_date integer, pokersite_id smallint )`);

//         let queryToCompiled = 'sum(winnings) as winnings,';
//         for (let i = 0; i < compiledColumns.length; i += 1) {
//             if (compiledColumns[i].data_type === 'integer' && compiledColumns[i].column_name !== 'bigblindincents' && compiledColumns[i].column_name !== 'playedyearandmonth') {
//                 if (i !== compiledColumns.length - 2) {
//                     queryToCompiled = `${queryToCompiled} SUM(${compiledColumns[i].column_name}) as ${compiledColumns[i].column_name}, `;
//                 } else {
//                     queryToCompiled = `${queryToCompiled} SUM(${compiledColumns[i].column_name}) as ${compiledColumns[i].column_name}`;
//                 }
//             } else if (compiledColumns[i].column_name === 'playedyearandmonth') {
//                 queryToCompiled = `${queryToCompiled} MIN(${compiledColumns[i].column_name}) as ${compiledColumns[i].column_name}, `;
//             }
//         }

//         // const players = await end_point_db.many('select * from players');
//         const players = await end_point_db.many(`select playername, pokersite_id from ${tableName} group by playername, pokersite_id order by playername `);
//         const playersCount = players.length;


//         for (let i = 0; i < playersCount; i += 1) {
//             // if (i < 480012) {
//             //     continue;
//             // }
//             // if (players[i].playername !== 'fobot21') {
//             //     continue;
//             // }
//             console.log('insert ', tableIndex, '/', s, ':', players[i].playername, i, ' / ', playersCount);

//             const r = await end_point_db.one(`SELECT ${queryToCompiled} from (
//             select *, ((select cast((totalamountwonincents) as float)) / 100 * currencies.value)::numeric(10,2) as winnings from ${tableName}
//                 join currencies on ${tableName}.currencytype_id = currencies.currency_id
//                 and ${tableName}.playedyearandmonth = currencies.hm_format_date
//                 where playername=$1 and pokersite_id=$2
//         ) as a group by playername`, [players[i].playername, players[i].pokersite_id])
//                 .catch((err) => {
//                     // console.log(err);
//                 });

//             if (!r) {
//                 continue;
//             }

//             // await end_point_db.any(
//             //     `INSERT INTO pre_${process.env.COMPILED_RESULTS_TABLE_NAME} (playername, pokersite_id, stats) values ($1, $2, $3)`,
//             //     [players[i].playername, players[i].pokersite_id, JSON.stringify(r)],
//             // );


//             // console.log(`pre_${compiledTable.table_name}`);
//             // await end_point_db.any(`INSERT INTO pre_${compiledTable.table_name} (playername, stats, pokersite_id) VALUES ($1,$2, $3)`, [players[i].playername, str, players[i].pokersite_id]);

//             // const json = JSON.parse(str);
//             // console.log(str, json);

//             const hands = +r.totalhands;
//             let blockSize = Math.floor(hands / 5000);
//             if (blockSize === 0) {
//                 blockSize = 1;
//             }

//             let result = await end_point_db.many(
//                 `
//                             select cast (rnum / $3 as int) as num, sum(result) as result, MIN(hm_format_date) as hm_format_date from (
//                                 select row_number() OVER () as rnum, result, hm_format_date from (
//                                 select result, hm_format_date from (
//                                     select hand_id, result, hm_format_date from ${formatedTableName} where playername = $1 and pokersite_id = $2
//                             ) as a order by hand_id) as b) as c group by num
//                         `
//                 , [players[i].playername, players[i].pokersite_id, blockSize],
//             ).catch(() => {

//             });

//             if (!result) {
//                 const pointsCount = +r.totalhands / 10 >= 1 ? 10 : +r.totalhands;
//                 const min = -100;
//                 const max = 100;
//                 let sum = 0;
//                 const gr = [];
//                 for (let m = 0; m < pointsCount; m += 1) {
//                     const k = Math.round(-0.5 + Math.random() * (100 + 1));
//                     const r1 = Math.round(min - 0.5 + Math.random() * (max - min + 1)) * k;
//                     sum += r1;
//                     gr.push(r1);
//                 }

//                 const pointWeight = +r.winnings / sum;

//                 result = gr.map((x, index) => ({
//                     num: index + 1,
//                     result: x * pointWeight,
//                     hm_format_date: r.playedyearandmonth,
//                 }));
//             } else {
//                 let sum = 0;
//                 for (const res of result) {
//                     sum += +res.result;
//                 }
//                 const diff = (+r.winnings - sum) / result.length;
//                 for (const res of result) {
//                     res.result = +res.result + diff;
//                 }
//             }

//             result = result.map(x => x.result);

//             // await end_point_db.any(`INSERT INTO ${preFormatedTableName} (result,playername,hm_format_date, pokersite_id) VALUES ($1, $2, $3,$4)
//             // `, [JSON.stringify(result), players[i].playername, r.playedyearandmonth, players[i].pokersite_id]);

//             const queries = result.map(res => end_point_db.any(`
//                 INSERT INTO ${preFormatedTableName} (num,result,playername,hm_format_date, pokersite_id) VALUES ($1, $2, $3,$4, $5)
//                 `, [res.num, res.result, players[i].playername, res.hm_format_date, players[i].pokersite_id]));

//             await Promise.all(queries);
//         }
//     }
// }

async function createNormilizedEndGraph() {
    // await end_point_db.any('drop table if exists normalized_compiled_player_results');
    // await end_point_db.any('create table normalized_compiled_player_results (id serial primary key, player_id integer, stats text)');

    await end_point_db.any('drop table if exists normalized_player_results');
    await end_point_db.any('create table normalized_player_results (id serial primary key, playername text, pokersite_id smallint, graph text)');

    const preFormated = await end_point_db.many('SELECT table_name FROM information_schema.tables WHERE table_schema=\'public\' AND table_type=\'BASE TABLE\' AND table_name like \'pre_formatedplayerresults%\'');
    // const preCompiled = await end_point_db.many('SELECT table_name FROM information_schema.tables WHERE table_schema=\'public\' AND table_type=\'BASE TABLE\' AND table_name like \'pre_mycompiledplayerresults%\'');

    const players = await end_point_db.many('select * from players order by playername');
    const playersCount = players.length;

    let queryToDb = '';
    for (let j = 0; j < preFormated.length; j += 1) {
        const query = `select num, hm_format_date, result
        from ${preFormated[j].table_name}
        where playername = $1 and pokersite_id = $2
        `;
        if (j + 1 !== preFormated.length) {
            queryToDb = `${queryToDb} ${query} union`;
        } else {
            queryToDb = `${queryToDb} ${query}`;
        }
    }
    const blockSize = 2000;

    const promises = [];
    for (let i = 0; i < playersCount; i += 1) {
        console.log('insert ', players[i].playername, i, ' / ', playersCount);


        // const count = await end_point_db.one(`select count(*) from (${queryToDb}) as a`, [players[i].playername, players[i].pokersite_id]);

        // const blocks = +count.count;

        // let blockSize = Math.floor(blocks / 50) + 1;
        // if (blockSize === 0) {
        //     blockSize = 1;
        // }

        // promises.push(end_point_db.any(
        //     `
        //     select result  from (
        //     ${queryToDb}
        // ) as a order by hm_format_date, num
        // `
        //     , [players[i].playername, players[i].pokersite_id, blockSize],
        // ));

        const resultToCash = await end_point_db.any(
            `
        select cast (rnum / $3 as int) as num, sum(result) from (
            select row_number() OVER () as rnum, result from (
            select result  from (
            ${queryToDb}
        ) as a order by hm_format_date, num) as b) as c group by num
        `
            , [players[i].playername, players[i].pokersite_id, blockSize],
        );

        const graphArray = [];
        for (const res of resultToCash) {
            graphArray.push(res.sum);
        }


        await end_point_db.any('insert into normalized_player_results (playername, pokersite_id, graph) values($1, $2, $3)', [players[i].playername, players[i].pokersite_id, graphArray.toString()]);

        // if (i % 100 === 0) {
        //     const res = await Promise.all(promises);

        //     promises = [];
        // }
    }
}


end_point_db.connect()
    .then(() => work_db.connect())
    .then(() => end_point_db.any(`create table if not exists cash(
            id serial primary key,
            playername text,
            pokersite_id smallint,
            stats text
        )`))
    .then(() => end_point_db.any(`
        create table if not exists users(
            id serial primary key,
            email text,
            password text,
            salt text,
            auth_secret text,
            is_registered boolean,
            is_admin boolean,
            expiration_subscription_date bigint,
            number_of_searches integer,
            subscription_type integer,
            last_token text
        )
    `))
    .then(() => end_point_db.any(`
        create table if not exists registration_tokens(
            token text primary key,
            status text,
            user_id integer
        )
    `))
    .then(() => end_point_db.any(`DROP table IF EXISTS ${compiledResultsTableName}`))
    .then(() => end_point_db.any(`create table ${compiledResultsTableName}(
            playername text,
            pokersite_id smallint,

            tablesize smallint,
            bigblindincents integer,
            playedyearandmonth integer,
            currencytype_id smallint,
            pokergametype_id smallint,

            totalhands integer NOT NULL,
            totalamountwonincents integer NOT NULL,
            totalrakeincents integer NOT NULL,
            totalbbswon integer NOT NULL,
            vpiphands integer NOT NULL,
            pfrhands integer NOT NULL,
            couldcoldcall integer NOT NULL,
            didcoldcall integer NOT NULL,
            couldthreebet integer NOT NULL,
            didthreebet integer NOT NULL,
            couldsqueeze integer NOT NULL,
            didsqueeze integer NOT NULL,
            facingtwopreflopraisers integer NOT NULL,
            calledtwopreflopraisers integer NOT NULL,
            raisedtwopreflopraisers integer NOT NULL,
            smallblindstealattempted integer NOT NULL,
            smallblindstealdefended integer NOT NULL,
            smallblindstealreraised integer NOT NULL,
            bigblindstealattempted integer NOT NULL,
            bigblindstealdefended integer NOT NULL,
            bigblindstealreraised integer NOT NULL,
            sawnonsmallshowdown integer NOT NULL,
            wonnonsmallshowdown integer NOT NULL,
            sawlargeshowdown integer NOT NULL,
            wonlargeshowdown integer NOT NULL,
            sawnonsmallshowdownlimpedflop integer NOT NULL,
            wonnonsmallshowdownlimpedflop integer NOT NULL,
            sawlargeshowdownlimpedflop integer NOT NULL,
            wonlargeshowdownlimpedflop integer NOT NULL,
            wonhand integer NOT NULL,
            wonhandwhensawflop integer NOT NULL,
            wonhandwhensawturn integer NOT NULL,
            wonhandwhensawriver integer NOT NULL,
            facedthreebetpreflop integer NOT NULL,
            foldedtothreebetpreflop integer NOT NULL,
            calledthreebetpreflop integer NOT NULL,
            raisedthreebetpreflop integer NOT NULL,
            facedfourbetpreflop integer NOT NULL,
            foldedtofourbetpreflop integer NOT NULL,
            calledfourbetpreflop integer NOT NULL,
            raisedfourbetpreflop integer NOT NULL,
            turnfoldippassonflopcb integer NOT NULL,
            turncallippassonflopcb integer NOT NULL,
            turnraiseippassonflopcb integer NOT NULL,
            riverfoldippassonturncb integer NOT NULL,
            rivercallippassonturncb integer NOT NULL,
            riverraiseippassonturncb integer NOT NULL,
            sawflop integer NOT NULL,
            sawshowdown integer NOT NULL,
            wonshowdown integer NOT NULL,
            totalbets integer NOT NULL,
            totalcalls integer NOT NULL,
            flopcontinuationbetpossible integer NOT NULL,
            flopcontinuationbetmade integer NOT NULL,
            turncontinuationbetpossible integer NOT NULL,
            turncontinuationbetmade integer NOT NULL,
            rivercontinuationbetpossible integer NOT NULL,
            rivercontinuationbetmade integer NOT NULL,
            facingflopcontinuationbet integer NOT NULL,
            foldedtoflopcontinuationbet integer NOT NULL,
            calledflopcontinuationbet integer NOT NULL,
            raisedflopcontinuationbet integer NOT NULL,
            facingturncontinuationbet integer NOT NULL,
            foldedtoturncontinuationbet integer NOT NULL,
            calledturncontinuationbet integer NOT NULL,
            raisedturncontinuationbet integer NOT NULL,
            facingrivercontinuationbet integer NOT NULL,
            foldedtorivercontinuationbet integer NOT NULL,
            calledrivercontinuationbet integer NOT NULL,
            raisedrivercontinuationbet integer NOT NULL,
            totalpostflopstreetsseen integer NOT NULL,
            totalaggressivepostflopstreetsseen integer NOT NULL,

            PRIMARY KEY (playername, pokersite_id, tablesize, bigblindincents, playedyearandmonth, currencytype_id, pokergametype_id)
        )`))
    .then(() => end_point_db.any(`DROP table IF EXISTS ${formatedPlayerResultsTableName}`))
    .then(() => end_point_db.any(`create table IF NOT EXISTS ${formatedPlayerResultsTableName}(
        id serial,
        playername text,
        result double precision,
        hm_format_date bigint,
        currencytype_id smallint,
        pokergametype_id smallint,
        pokersite_id smallint,
        bigblindincents integer,
        tablesize smallint,
        hand_id bigint
        )`))
    .then(() => convertToNormalizedPlayerNames())
    .then(() => insertCompiledPlayerResults())
    .then(() => insertFormatedPlayerResults())
    .then(() => convertToNormalizedPlayerNames())
    .then(() => generatePlayerNames())
    .then(() => createNormilizedGraph())
    // .then(() => createNormilizedEndGraph())
    .then(() => end_point_db.any('delete from cash'))
    .then(() => {
        process.exit();
    })
    .catch((err) => {
        console.log(err);
    });

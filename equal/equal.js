require('dotenv').config();
const pgp = require('pg-promise')({});

const end_point_cn = {
    host: process.env.DB_HOST,
    port: process.env.DB_PORT,
    database: process.env.DB_END_POINT_DB_NAME,
    user: process.env.DB_USER,
    password: process.env.DB_PASS,
};

const end_point_db = pgp(end_point_cn);

async function start() {
    const playersFromHmDb = await end_point_db.many(`select playername, pokersite_id from mycompiledplayerresults_201802nl25sh
     group by playername, pokersite_id order by playername`);

    const countPlayers = playersFromHmDb.length;
    // for (const player of playersFromHmDb) {
    for (let i = 0; i < countPlayers; i += 1) {
        const player = playersFromHmDb[i];
        const playername = player.playername;

        // if (playername !== 'AnalniyPirat') {
        //     continue;
        // }

        console.log(`${i} / ${countPlayers} - ${playername}`);

        const hmResults
            = await end_point_db.many('select playername, tablesize, bigblindincents, playedyearandmonth, currencytype_id, pokergametype_id, totalamountwonincents as wins  from mycompiledplayerresults_201802nl25sh where playername = $1', [playername]);

        for (const hmResult of hmResults) {
            const {
                tablesize, bigblindincents, playedyearandmonth, currencytype_id, pokergametype_id,
            } = hmResult;
            const resultFromWorkDb
                = await end_point_db.one(
                    `select sum(result) as wins from formatedplayerresults_201802nl25sh 
                     where playername = $1 and tablesize = $2 and bigblindincents = $3 and hm_format_date=$4 
                     and currencytype_id= $5 and pokergametype_id=$6`,
                    [playername, tablesize, bigblindincents, playedyearandmonth, currencytype_id, pokergametype_id],
                );
            const countFromWorkDb
                = await end_point_db.one(
                    `select count(*) from formatedplayerresults_201802nl25sh 
                    where playername = $1 and tablesize = $2 and bigblindincents = $3 and hm_format_date=$4 
                     and currencytype_id= $5 and pokergametype_id=$6`,
                    [playername, tablesize, bigblindincents, playedyearandmonth, currencytype_id, pokergametype_id],
                );

            const fromHM = +hmResult.wins / 100;
            const fromDb = +resultFromWorkDb.wins;

            const diff = fromHM - fromDb;
            const count = +countFromWorkDb.count;

            // const diffToUpdate = Math.round(diff / count * 100) / 100;
            const diffToUpdate = diff / count;

            await end_point_db.any(
                `update formatedplayerresults_201802nl25sh set result = result + $7 
            where playername = $1 and tablesize = $2 and bigblindincents = $3 and hm_format_date=$4 
                     and currencytype_id= $5 and pokergametype_id=$6`,
                [playername, tablesize, bigblindincents, playedyearandmonth, currencytype_id, pokergametype_id, diffToUpdate],
            );
        }


        // const resultFromHmDb = await end_point_db.one('select sum(totalamountwonincents) as wins from mycompiledplayerresults_201802nl25sh where playername = $1', [playername]);
        // const resultFromWorkDb = await end_point_db.one('select sum(result) as wins from formatedplayerresults_201802nl25sh where playername = $1', [playername]);
        // const countFromWorkDb = await end_point_db.one('select count(*) from formatedplayerresults_201802nl25sh where playername = $1', [playername]);


        // const fromHM = +resultFromHmDb.wins / 100;
        // const fromDb = +resultFromWorkDb.wins;


        // const diff = fromHM - fromDb;
        // const count = +countFromWorkDb.count;

        // // await end_point_db.any('update formatedplayerresults_201802nl25sh set result = $1 where playername = $2', [fromHM / count, playername]);

        // if (count === 0 || diff === 0) {
        //     continue;
        // }

        // const diffToUpdate = Math.round(diff / count * 100) / 100;
        // // console.log(diffToUpdate);
        // await end_point_db.any('update formatedplayerresults_201802nl25sh set result = result + $1 where playername = $2', [diffToUpdate, playername]);
    }
}


start();
